#!/bin/bash

unset NOW
unset FILENAME
unset OUTPUTDIR

clear
echo "----------------------------------------------------------"
echo "AKAB = automatische Bild- anonymisierung und komprimierung"
echo "----------------------------------------------------------"
read -p "Dateiname: " FILENAME
read -p "Bilder Ordner: " PICDIR
read -p "Output Ordner: " OUTPUTDIR
echo "----------------------------------------------------------"

NOW=$(date +"%Y_%m_%d")
mkdir -p $OUTPUTDIR

convert -verbose -resize 1920x1080! -geometry 1920x1080 -sharpen 1 -quality 100 $PICDIR/* $OUTPUTDIR/$FILENAME-$NOW.jpg
echo -e "\e[32mconvert complete"
mat2 --inplace --verbose $OUTPUTDIR/*
echo -e "\e[39m----------------------------------------------------------"
echo -e "\e[32mmat2 complete"
echo -e "\e[39m----------------------------------------------------------"
echo -e "\e[39mOutput-dir: $OUTPUTDIR"
echo -e "\e[39m----------------------------------------------------------"

###
unset NOW
unset FILENAME
unset OUTPUTDIR

exit 0
#EOF
